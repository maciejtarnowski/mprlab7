package builders;

import java.util.List;

import models.ChartSerie;
import models.ChartSettings;
import models.ChartType;

public class ChartBuilder {
	private ChartSettings chartSettings = new ChartSettings();
	
	public ChartBuilder addSerie(ChartSerie serie) {
		chartSettings.getSeries().add(serie);
		
		return this;
	}
	
	public ChartBuilder withSeries(List<ChartSerie> series) {
		chartSettings.setSeries(series);
		
		return this;
	}
	
	public ChartBuilder withTitle(String title) {
		chartSettings.setTitle(title);
		
		return this;
	}
	
	public ChartBuilder withLegendState(boolean state) {
		chartSettings.setLegend(state);
		
		return this;
	}
	
	public ChartBuilder withType(ChartType type) {
		chartSettings.setChartType(type);
		
		return this;
	}
	
	public ChartSettings build() {
		return chartSettings;
	}
}

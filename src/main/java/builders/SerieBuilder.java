package builders;

import java.util.List;

import models.ChartSerie;
import models.Point;
import models.SerieType;

public class SerieBuilder {
	private ChartSerie chartSerie = new ChartSerie();
	
	public SerieBuilder addPoint(Point point) {
		chartSerie.getPoints().add(point);
		
		return this;
	}
	
	public SerieBuilder withLabel(String label) {
		chartSerie.setLabel(label);
		
		return this;
	}
	
	public SerieBuilder withPoints(List<Point> points) {
		chartSerie.setPoints(points);
		
		return this;
	}
	
	public SerieBuilder withType(SerieType type) {
		chartSerie.setSerieType(type);
		
		return this;
	}
	
	public ChartSerie build() {
		return chartSerie;
	}
}

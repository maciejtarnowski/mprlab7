package models;

import java.util.ArrayList;
import java.util.List;

public class ChartSettings {
	private List<ChartSerie> series;
	private String title;
	private String subtitle;
	private boolean hasLegend;
	private ChartType chartType;
	
	public List<ChartSerie> getSeries() {
		if (series == null) {
			series = new ArrayList<ChartSerie>();
		}
		return series;
	}
	
	public void setSeries(List<ChartSerie> series) {
		this.series = series;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getSubtitle() {
		return subtitle;
	}
	
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	
	public boolean hasLegend() {
		return hasLegend;
	}
	
	public void setLegend(boolean hasLegend) {
		this.hasLegend = hasLegend;
	}
	
	public ChartType getChartType() {
		return chartType;
	}
	
	public void setChartType(ChartType chartType) {
		this.chartType = chartType;
	}
}

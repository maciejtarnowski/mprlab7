package models;

import java.util.ArrayList;
import java.util.List;

public class ChartSerie {
	private String label;
	private List<Point> points;
	private SerieType serieType;
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public List<Point> getPoints() {
		if (points == null) {
			points = new ArrayList<Point>();
		}
		return points;
	}
	
	public void setPoints(List<Point> points) {
		this.points = points;
	}
	
	public SerieType getSerieType() {
		return serieType;
	}
	
	public void setSerieType(SerieType serieType) {
		this.serieType = serieType;
	}
}

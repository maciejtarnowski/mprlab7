package builders;

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import models.ChartSerie;
import models.Point;
import models.SerieType;

@RunWith(MockitoJUnitRunner.class)
public class SerieBuilderTest {
	private SerieBuilder serieBuilder;
	
	@Mock
	private Point point;
	
	@Mock
	private List<Point> points;
	
	@Before
	public void setUp() {
		serieBuilder = new SerieBuilder();
	}
	
	@After
	public void tearDown() {
		serieBuilder = null;
	}
	
	@Test
	public void itAddsPoint() {
		ChartSerie chartSerie = serieBuilder.addPoint(point).build();
		
		assertThat(chartSerie.getPoints()).containsExactly(point);
	}
	
	@Test
	public void itSetsLabel() {
		ChartSerie chartSerie = serieBuilder.withLabel("Serie label").build();
		
		assertThat(chartSerie.getLabel()).isEqualTo("Serie label");
	}
	
	@Test
	public void itSetsPoints() {
		ChartSerie chartSerie = serieBuilder.withPoints(points).build();
		
		assertThat(chartSerie.getPoints()).isEqualTo(points);
	}
	
	@Test
	public void itSetsType() {
		ChartSerie chartSerie = serieBuilder.withType(SerieType.Bar).build();
		
		assertThat(chartSerie.getSerieType()).isEqualTo(SerieType.Bar);
	}
	
	@Test
	public void itImplementsFluentInterface() {
		ChartSerie chartSerie = serieBuilder
				.addPoint(point)
				.withLabel("Serie label")
				.withType(SerieType.LinePoint)
				.build();
		
		assertThat(chartSerie).isInstanceOf(ChartSerie.class);
		assertThat(chartSerie.getPoints()).containsExactly(point);
		assertThat(chartSerie.getLabel()).isEqualTo("Serie label");
		assertThat(chartSerie.getSerieType()).isEqualTo(SerieType.LinePoint);
	}
}

package builders;

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import models.ChartSerie;
import models.ChartSettings;
import models.ChartType;

@RunWith(MockitoJUnitRunner.class)
public class ChartBuilderTest {
	private ChartBuilder chartBuilder;
	
	@Mock
	private ChartSerie chartSerie;
	
	@Mock
	private List<ChartSerie> chartSeries;
	
	@Before
	public void setUp() {
		chartBuilder = new ChartBuilder();
	}
	
	@After
	public void tearDown() {
		chartBuilder = null;
	}
	
	@Test
	public void itAddsSeries() {
		ChartSettings chartSettings = chartBuilder.addSerie(chartSerie).build();
		
		assertThat(chartSettings.getSeries()).containsExactly(chartSerie);
	}
	
	@Test
	public void itSetsSeries() {
		ChartSettings chartSettings = chartBuilder.withSeries(chartSeries).build();
		
		assertThat(chartSettings.getSeries()).isEqualTo(chartSeries);
	}
	
	@Test
	public void itSetsTitle() {
		ChartSettings chartSettings = chartBuilder.withTitle("Chart title").build();
		
		assertThat(chartSettings.getTitle()).isEqualTo("Chart title");
	}
	
	@Test
	public void itSetsLegendState() {
		ChartSettings chartSettings = chartBuilder.withLegendState(true).build();
		
		assertThat(chartSettings.hasLegend()).isTrue();
	}
	
	@Test
	public void itSetsType() {
		ChartSettings chartSettings = chartBuilder.withType(ChartType.Area).build();
		
		assertThat(chartSettings.getChartType()).isEqualTo(ChartType.Area);
	}
	
	@Test
	public void itImplementsFluentInterface() {
		ChartSettings chartSettings = chartBuilder
				.addSerie(chartSerie)
				.withTitle("Chart title")
				.withLegendState(false)
				.withType(ChartType.Line)
				.build();
		
		assertThat(chartSettings).isInstanceOf(ChartSettings.class);
		assertThat(chartSettings.getSeries()).containsExactly(chartSerie);
		assertThat(chartSettings.getTitle()).isEqualTo("Chart title");
		assertThat(chartSettings.hasLegend()).isFalse();
		assertThat(chartSettings.getChartType()).isEqualTo(ChartType.Line);
	}
}

package models;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ChartSerieTest {
	private ChartSerie chartSerie;
	
	@Mock
	private List<Point> points;
	
	@Before
	public void setUp() {
		chartSerie = new ChartSerie();
	}
	
	@After
	public void tearDown() {
		chartSerie = null;
	}
	
	@Test
	public void itReturnsEmptyArrayListIfPointsListIsNull() {
		chartSerie.setPoints(null);
		
		assertThat(chartSerie.getPoints()).isInstanceOf(ArrayList.class).isEmpty();
	}
	
	@Test
	public void itReturnsSetListIfPointsListIsNotNull() {
		chartSerie.setPoints(points);
		
		assertThat(chartSerie.getPoints()).isEqualTo(points);
	}
}

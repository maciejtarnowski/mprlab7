package models;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ChartSettingsTest {
	private ChartSettings chartSettings;
	
	@Mock
	private List<ChartSerie> series;
	
	@Before
	public void setUp() {
		chartSettings = new ChartSettings();
	}
	
	@After
	public void tearDown() {
		chartSettings = null;
	}
	
	@Test
	public void itReturnsEmptyArrayListIfSerieListIsNull() {
		chartSettings.setSeries(null);
		
		assertThat(chartSettings.getSeries()).isInstanceOf(ArrayList.class).isEmpty();
	}
	
	@Test
	public void itReturnsSetListIfSeriesListIsNotNull() {
		chartSettings.setSeries(series);
		
		assertThat(chartSettings.getSeries()).isEqualTo(series);
	}
}
